import cv2
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import moviepy.editor as mp
from datetime import timedelta

def format_time(time_input):
    parts = time_input.split(":")
    minutes = int(parts[0])
    seconds = int(parts[1])
    return minutes * 60 + seconds

def cut_video(input_file, start_time, end_time):
    output_file = "output.mp4"  # Nama file video hasil potongan
    
    if start_time is None:
        start_time_formatted = 0
    else:
        start_time_formatted = start_time

    if end_time is None:
        duration = None
    else:
        duration = end_time - start_time_formatted
        
    ffmpeg_extract_subclip(input_file, start_time_formatted, duration, targetname=output_file)
    print("Video berhasil dipotong dan disimpan sebagai", output_file)

    audio_file = input("Masukkan nama file audio yang ingin ditambahkan: ")

    add_audio(output_file, audio_file, start_time_formatted, input_file)

def add_audio(video_file, audio_file, start_time, input_file):
    video = mp.VideoFileClip(video_file)
    audio = mp.AudioFileClip(audio_file)

    if start_time is not None:
        # Memotong audio dari waktu mulai yang diinginkan
        audio = audio.subclip(start_time)  

    # Sinkronkan durasi audio dengan durasi video
    audio = audio.set_duration(video.duration)

    video = video.set_audio(audio)
    
    # Ubah nama output_file sesuai dengan nama file input
    output_file = input_file.split(".")[0] + "_with_audio.mp4"
    video.write_videofile(output_file, codec='libx264', audio_codec='aac')
    print("Video berhasil ditambahkan audio dan disimpan sebagai", output_file)

# Meminta input dari pengguna
input_file = input("Masukkan nama file video: ")
start_time_input = input("Masukkan waktu mulai (MM:SS) : ")
end_time_input = input("Masukkan waktu berakhir (MM:SS) : ")

# Mengatur nilai start_time dan end_time berdasarkan input pengguna
if start_time_input == "":
    start_time = None
else:
    start_time = format_time(start_time_input)

if end_time_input == "":
    end_time = None
else:
    end_time = format_time(end_time_input)

cut_video(input_file, start_time, end_time)
